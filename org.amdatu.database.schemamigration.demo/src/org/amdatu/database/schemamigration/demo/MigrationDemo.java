/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.database.schemamigration.demo;

import javax.sql.DataSource;

import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.osgi.framework.BundleContext;

public class MigrationDemo {

	private volatile DataSource m_ds;
	private volatile SchemaMigrationService m_migrationService;
	private volatile BundleContext m_bundleContext;

	public void start() {
		m_migrationService.migrate(m_ds, m_bundleContext.getBundle(), "migrations");

		System.out.println("Migration completed!");
	}
}
