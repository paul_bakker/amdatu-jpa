/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.database.schemamigration;

import javax.sql.DataSource;

import org.osgi.framework.Bundle;

/**
 * Service that performs database schema migrations
 *
 */
public interface SchemaMigrationService {
	/**
	 * @param ds DataSource to use. This is explicitly passed in because there might be multiple data sources available.
	 * @param bundle Bundle that contains the migration scripts.
	 * @param migrationScriptDir Directory in the bundle that contains migration scripts.
	 * 
	 * @return String the current version of the schema
	 */
	String migrate(DataSource dataSource, Bundle bundle, String migrationScriptDir);

	/**
	 * @param ds DataSource to use. This is explicitly passed in because there might be multiple data sources available.
	 * @param bundle Bundle that contains the migration scripts.
	 * @param schemaName String name of the schema
	 * @param migrationScriptDir Directory in the bundle that contains migration scripts.
	 * 
	 * @return String the current version of the schema
	 */
	String migrate(DataSource dataSource, Bundle bundle, String schemaName, String migrationScriptDir);
	
	/**
	 * @param ds DataSource to use. This is explicitly passed in because there might be multiple data sources available.
	 * @param migrationScriptDir List of absolute paths on disk to directories containing migration scripts.
	 * 
	 * @return String the current version of the schema
	 */
	String migrate(DataSource dataSource, String... scriptPaths);

}
