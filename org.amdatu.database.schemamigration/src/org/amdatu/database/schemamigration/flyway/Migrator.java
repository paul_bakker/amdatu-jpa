/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.database.schemamigration.flyway;

import java.io.File;
import java.util.Enumeration;

import javax.sql.DataSource;

import org.amdatu.database.schemamigration.SchemaMigrationException;
import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.flywaydb.core.Flyway;
import org.osgi.framework.Bundle;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.log.LogService;

public class Migrator implements SchemaMigrationService {
	private volatile LogService m_logService;

	@Override
	public String migrate(DataSource ds, Bundle bundle, String migrationScriptDir) {
		return migrate(ds, bundle, null, migrationScriptDir);
	}
	
	@Override
	public String migrate(DataSource ds, Bundle bundle, String schemaName, String migrationScriptDir) {
		m_logService.log(LogService.LOG_INFO, "Start Flyway migration of bundle " + bundle.getSymbolicName());
		File migrationsDir = bundle.getDataFile(migrationScriptDir);
		migrationsDir.mkdir();

		Enumeration<String> entryPaths = bundle.getEntryPaths(migrationScriptDir);
		if(entryPaths == null) {
			throw new SchemaMigrationException("Directory '" + migrationScriptDir + "' doesn't exist");
		}
		
		//TODO: For some reason this failed using javac in a gradle build without the explicit cast...
		ClassLoader bundleClassLoader = ((BundleWiring)bundle.adapt(BundleWiring.class)).getClassLoader();
		return migrate(ds, bundleClassLoader, schemaName, migrationScriptDir);
	}
	
	@Override
	public String migrate(DataSource ds, String... scriptPaths) {
		String[] scriptUrls = new String[scriptPaths.length];
		for (int i = 0; i < scriptPaths.length; i++) {
			scriptUrls[i] = "filesystem:" + scriptPaths[i];
		}

		return migrate(ds, Flyway.class.getClassLoader(), null, scriptUrls);
	}
	
	private String migrate(DataSource ds, ClassLoader cl, String schemaName, String... scriptPaths) {	
		try {
			Flyway flyway = new Flyway();
			
			flyway.setClassLoader(cl);
			flyway.setDataSource(ds);
			flyway.setLocations(scriptPaths);
			flyway.setInitOnMigrate(true);
			if (schemaName != null){
				flyway.setTable(schemaName.concat("_schema_version"));
			}
			
			flyway.migrate();
			
			return flyway.info().current().getVersion().toString();
		} catch (Exception e) {
			throw new SchemaMigrationException(e);
		}
	}
	
}
