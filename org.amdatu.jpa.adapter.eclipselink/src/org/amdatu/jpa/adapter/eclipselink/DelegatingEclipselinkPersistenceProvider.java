/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.adapter.eclipselink;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.persistence.spi.ProviderUtil;
import javax.sql.DataSource;

import org.amdatu.jpa.adapter.PersistenceProviderAdapter;

public class DelegatingEclipselinkPersistenceProvider implements PersistenceProvider, PersistenceProviderAdapter {

	private final PersistenceProvider m_delegate = new org.eclipse.persistence.jpa.PersistenceProvider();

	
	@SuppressWarnings("unchecked")
	public EntityManagerFactory createContainerEntityManagerFactory(
			PersistenceUnitInfo info, @SuppressWarnings("rawtypes") Map properties) {
		
		@SuppressWarnings("rawtypes")
		Map p = new HashMap(); 
		if (properties != null){
			p.putAll(properties);
		}
		if (!properties.containsKey("eclipselink.target-server")){
			p.put("eclipselink.target-server", JtaTransactionControllerImpl.class.getName());
		}
		
		return m_delegate.createContainerEntityManagerFactory(new PuInfoWrapper(info), p);
		
	}

	public EntityManagerFactory createEntityManagerFactory(String emName,
			@SuppressWarnings("rawtypes") Map properties) {
		throw new IllegalStateException("Not supported");
	}

	public boolean equals(Object obj) {
		return m_delegate.equals(obj);
	}

	public void generateSchema(PersistenceUnitInfo info, @SuppressWarnings("rawtypes") Map properties) {
		m_delegate.generateSchema(info, properties);
	}

	public boolean generateSchema(String persistenceUnitName, @SuppressWarnings("rawtypes") Map properties) {
		return m_delegate.generateSchema(persistenceUnitName, properties);
	}

	public ProviderUtil getProviderUtil() {
		return m_delegate.getProviderUtil();
	}

	public int hashCode() {
		return m_delegate.hashCode();
	}

	public String toString() {
		return m_delegate.toString();
	}

	/**
	 *  {@link PersistenceUnitInfo} wrapper used to override the ClassLoader and 
	 *  make {@link JtaTransactionControllerImpl} visible to EclipseLink
	 */
	class PuInfoWrapper implements PersistenceUnitInfo {
		
		private final PersistenceUnitInfo m_delegate;

		public PuInfoWrapper(PersistenceUnitInfo m_delegate) {
			super();
			this.m_delegate = m_delegate;
		}

		public void addTransformer(ClassTransformer arg0) {
			m_delegate.addTransformer(arg0);
		}

		public boolean excludeUnlistedClasses() {
			return m_delegate.excludeUnlistedClasses();
		}

		public ClassLoader getClassLoader() {
			return new ClassLoader(m_delegate.getClassLoader()) {
				
				@Override
				public Class<?> loadClass(String name) throws ClassNotFoundException {
					try {
						return super.loadClass(name);
					}catch (ClassNotFoundException e){
						return getClass().getClassLoader().loadClass(name);
					}
					
				}
				
			};
			
		}

		public List<URL> getJarFileUrls() {
			return m_delegate.getJarFileUrls();
		}

		public DataSource getJtaDataSource() {
			return m_delegate.getJtaDataSource();
		}

		public List<String> getManagedClassNames() {
			return m_delegate.getManagedClassNames();
		}

		public List<String> getMappingFileNames() {
			return m_delegate.getMappingFileNames();
		}

		public ClassLoader getNewTempClassLoader() {
			return m_delegate.getNewTempClassLoader();
		}

		public DataSource getNonJtaDataSource() {
			return m_delegate.getNonJtaDataSource();
		}

		public String getPersistenceProviderClassName() {
			return m_delegate.getPersistenceProviderClassName();
		}

		public String getPersistenceUnitName() {
			return m_delegate.getPersistenceUnitName();
		}

		public URL getPersistenceUnitRootUrl() {
			return m_delegate.getPersistenceUnitRootUrl();
		}

		public String getPersistenceXMLSchemaVersion() {
			return m_delegate.getPersistenceXMLSchemaVersion();
		}

		public Properties getProperties() {
			return m_delegate.getProperties();
		}

		public SharedCacheMode getSharedCacheMode() {
			return m_delegate.getSharedCacheMode();
		}

		public PersistenceUnitTransactionType getTransactionType() {
			return m_delegate.getTransactionType();
		}

		public ValidationMode getValidationMode() {
			return m_delegate.getValidationMode();
		}
		
	}
	
	@Override
	public String getDynamicImports() {
		return "*";
	}
}


