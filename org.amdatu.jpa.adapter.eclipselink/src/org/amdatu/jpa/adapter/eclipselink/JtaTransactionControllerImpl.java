/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.adapter.eclipselink;

import javax.transaction.TransactionManager;

import org.eclipse.persistence.exceptions.TransactionException;
import org.eclipse.persistence.transaction.JTATransactionController;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

public class JtaTransactionControllerImpl extends JTATransactionController {
		
	@Override
	protected TransactionManager acquireTransactionManager() throws Exception {
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		BundleContext bundleContext = bundle.getBundleContext();
		
		ServiceReference<TransactionManager> serviceReference = bundleContext.getServiceReference(TransactionManager.class);
		if (serviceReference != null){
			TransactionManager tm = bundleContext.getService(serviceReference);
			bundleContext.ungetService(serviceReference);
			if (tm != null){
				return tm; 
			}
		}
		
		throw new TransactionException("No transaction manager available");
	}
	
}
