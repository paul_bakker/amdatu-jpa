/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.hibernate;

import java.util.Properties;

import javax.persistence.spi.PersistenceProvider;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.hibernate.engine.transaction.jta.platform.spi.JtaPlatform;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1)
			throws Exception {
	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm)
			throws Exception {
		
		dm.add(createComponent().setInterface(JtaPlatform.class.getName(), null).setImplementation(JtaPlatformImpl.class)
				.add(createServiceDependency().setService(TransactionManager.class).setRequired(true))
				.add(createServiceDependency().setService(UserTransaction.class).setRequired(true)));
		
		Properties props = new Properties();
		props.put("javax.persistence.provider", HibernatePersistenceProvider.class.getName());
		props.put("javax.persistence.spi.PersistenceProvider", HibernatePersistenceProvider.class.getName());
		dm.add(createComponent().setInterface(PersistenceProvider.class.getName(), props).setImplementation(DelegatingHibernatePersistenceProvider.class)
				.add(createServiceDependency().setService(JtaPlatform.class).setRequired(true)));

	}

}
