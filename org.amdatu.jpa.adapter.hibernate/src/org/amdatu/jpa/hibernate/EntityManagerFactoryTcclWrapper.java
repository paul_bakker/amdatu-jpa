/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.hibernate;

import java.util.Collections;
import java.util.Map;

import javax.persistence.Cache;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;

public class EntityManagerFactoryTcclWrapper implements EntityManagerFactory {

	private final EntityManagerFactory m_delegate;
	
	private final ClassLoader m_classLoader;
	
	public EntityManagerFactoryTcclWrapper(EntityManagerFactory m_delegate, ClassLoader m_classLoader) {
		super();
		this.m_delegate = m_delegate;
		this.m_classLoader = m_classLoader;
	}

	public <T> void addNamedEntityGraph(String arg0, EntityGraph<T> arg1) {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			m_delegate.addNamedEntityGraph(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public void addNamedQuery(String arg0, Query arg1) {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			m_delegate.addNamedQuery(arg0, arg1);
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public void close() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			m_delegate.close();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public EntityManager createEntityManager() {
		return createEntityManager(SynchronizationType.UNSYNCHRONIZED, Collections.emptyMap());
	}

	public EntityManager createEntityManager(@SuppressWarnings("rawtypes") Map arg0) {
		return createEntityManager(SynchronizationType.UNSYNCHRONIZED, arg0);
	}

	public EntityManager createEntityManager(SynchronizationType arg0) {
		return createEntityManager(arg0, Collections.emptyMap());
	}
	
	public EntityManager createEntityManager(SynchronizationType arg0, @SuppressWarnings("rawtypes") Map arg1) {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			EntityManager em = new EntityManagerTcclWrapper(m_delegate.createEntityManager(), m_classLoader);
			return em;
			
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}


	public Cache getCache() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.getCache();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public CriteriaBuilder getCriteriaBuilder() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.getCriteriaBuilder();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public Metamodel getMetamodel() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.getMetamodel();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public PersistenceUnitUtil getPersistenceUnitUtil() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.getPersistenceUnitUtil();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public Map<String, Object> getProperties() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.getProperties();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public boolean isOpen() {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.isOpen();
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	}

	public <T> T unwrap(Class<T> arg0) {
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		try {
			Thread.currentThread().setContextClassLoader(m_classLoader);

			return m_delegate.unwrap(arg0);
		} finally {
			Thread.currentThread().setContextClassLoader(tccl);
		}
	} 
	
}
