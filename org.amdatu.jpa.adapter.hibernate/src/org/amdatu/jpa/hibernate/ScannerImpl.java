/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;

import javax.persistence.PersistenceException;

import org.hibernate.jpa.boot.archive.spi.ArchiveContext;
import org.hibernate.jpa.boot.archive.spi.ArchiveDescriptor;
import org.hibernate.jpa.boot.archive.spi.ArchiveDescriptorFactory;
import org.hibernate.jpa.boot.archive.spi.ArchiveEntry;
import org.hibernate.jpa.boot.scan.spi.AbstractScannerImpl;
import org.hibernate.jpa.boot.spi.InputStreamAccess;
import org.hibernate.jpa.boot.spi.NamedInputStream;
import org.osgi.framework.Bundle;

@SuppressWarnings("deprecation")
public class ScannerImpl extends AbstractScannerImpl {

	protected ScannerImpl(Bundle bundle) {
		super(new ArchiveDescriptorFactoryImpl(bundle));
	}

	private static final class ArchiveDescriptorFactoryImpl implements
			ArchiveDescriptorFactory {
		private final Bundle bundle;

		private ArchiveDescriptorFactoryImpl(Bundle bundle) {
			this.bundle = bundle;
		}

		@Override
		public URL getURLFromPath(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public URL getJarURLFromURLEntry(URL arg0, String arg1)
				throws IllegalArgumentException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ArchiveDescriptor buildArchiveDescriptor(URL arg0, String arg1) {
			return new ArchiveDescriptorImpl(bundle);
		}

		@Override
		public ArchiveDescriptor buildArchiveDescriptor(URL arg0) {
			return buildArchiveDescriptor(arg0, "");
		}
	}

	private static final class ArchiveDescriptorImpl implements
			ArchiveDescriptor {
		private final Bundle bundle;

		private ArchiveDescriptorImpl(Bundle bundle) {
			this.bundle = bundle;
		}

		@Override
		public void visitArchive(ArchiveContext context) {
			Enumeration<URL> entries = bundle.findEntries("/", "*", true);

			while (entries.hasMoreElements()) {
				final URL entry = entries.nextElement();

				if (entry.toString().endsWith("/")) {
					continue;
				}

				ArchiveEntry archiveEntry = new ArchiveEntry() {

					@Override
					public InputStreamAccess getStreamAccess() {
						return new InputStreamAccess() {

							@Override
							public String getStreamName() {
								return entry.getFile().toString();
							}

							@Override
							public NamedInputStream asNamedInputStream() {
								try {
									return new NamedInputStream(getStreamName(), entry.openStream());
								} catch (IOException e) {
									throw new PersistenceException(e);
								}
							}

							@Override
							public InputStream accessInputStream() {
								try {
									return entry.openStream();
								} catch (IOException e) {
									throw new PersistenceException(e);
								}
							}
						};
					}

					@Override
					public String getNameWithinArchive() {
						return entry.getFile().toString();
					}

					@Override
					public String getName() {
						return entry.getFile().toString();
					}
				};

				context.obtainArchiveEntryHandler(archiveEntry).handleEntry(archiveEntry, context);
			}
		}
	}

}
