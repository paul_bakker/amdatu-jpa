/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.datasourcefactory;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;
import javax.transaction.TransactionManager;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

public class DataSourceManagedServiceFactory implements ManagedServiceFactory {

	public static final String PID = "org.amdatu.jpa.datasourcefactory";

	private final ValidationQueryHelper m_validationQueryHelper = new ValidationQueryHelper();
	
	private volatile DependencyManager dependencyManager;

	private volatile LogService logService;

	private Map<String, Component> components = new HashMap<>();

	@Override
	public String getName() {
		return PID;
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {

		if (components.containsKey(pid)) {
			logService.log(LogService.LOG_INFO, "Removing datasouce " + pid);
			Component component = components.remove(pid);
			dependencyManager.remove(component);
		}

		String userName = getRequiredStringProperty(properties, "userName");
		String password = getRequiredStringProperty(properties, "password");
		String driverClassName = getRequiredStringProperty(properties, "driverClassName");
		String jdbcUrl = getRequiredStringProperty(properties, "jdbcUrl");
		String serviceName = getRequiredStringProperty(properties, "serviceName");
		
		String validationQuery = (String)properties.get("validationQuery");
		if (StringUtils.isEmpty(validationQuery)){
			validationQuery = m_validationQueryHelper.getValidationQuery(driverClassName);
		}
		
		logService
				.log(LogService.LOG_INFO, "Registering datasource [serviceName: " + serviceName + ", driverClass: " + driverClassName + "]");

		boolean managed = false;
		if (properties.get("managed") != null) {
			managed = BooleanUtils.toBooleanObject((String) properties.get("managed"));
		}
		
		BasicDataSource dataSource;
		if (managed) {
			dataSource = new ManagedDataSource();
		} else {
			dataSource = new BasicDataSource();
		}
		
		try {
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			throw new ConfigurationException("driverClassName", "Driver class " + driverClassName + " not found" );
		}
		
		dataSource.setDriverClassName(driverClassName);
		dataSource.setUsername(userName);
		dataSource.setPassword(password);
		dataSource.setUrl(jdbcUrl);
		dataSource.setDefaultAutoCommit(false);
		
		if (StringUtils.isNotBlank(validationQuery)){
			dataSource.setValidationQuery(validationQuery);
			dataSource.setTestOnBorrow(true);
		}else{
			logService.log(LogService.LOG_INFO, "DataSource validation disabled.");
			logService.log(LogService.LOG_INFO, "No validation query available or provided for driver class " + driverClassName );
		}
		


		Properties dsProps = new Properties();
		dsProps.put("name", serviceName);
		if (properties.get("tenant") != null) {
			dsProps.put("org.amdatu.tenant.pid",properties.get("tenant"));
		}
		Component component = dependencyManager.createComponent().setInterface(DataSource.class.getName(), dsProps)
				.setImplementation(dataSource);

		if (managed) {
			component.add(
					dependencyManager.createServiceDependency().setService(TransactionManager.class).setRequired(true)
							.setCallbacks(dataSource, "setTransactionManager", "setTransactionManager")).add(
					dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false));
		}

		dependencyManager.add(component);
		components.put(pid, component);

	}

	private String getRequiredStringProperty(Dictionary<String, ?> properties, String key)
			throws ConfigurationException {
		String userName = (String) properties.get(key);
		if (userName == null) {
			throw new ConfigurationException(key, "Property " + key + " is required");
		}
		return userName;
	}

	@Override
	public void deleted(String pid) {
		logService.log(LogService.LOG_INFO, "Unregisrering datasource with pid " + pid);
		dependencyManager.remove(components.remove(pid));
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private Object[] composition(){
		return new Object[]{this, m_validationQueryHelper};
	}

}
