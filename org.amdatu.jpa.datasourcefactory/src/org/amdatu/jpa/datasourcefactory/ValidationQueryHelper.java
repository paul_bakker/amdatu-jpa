/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.datasourcefactory;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class ValidationQueryHelper {

	private volatile BundleContext m_budleContext;
	
	private volatile LogService m_logService;

	private Properties m_defaultValidationQueries;
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void start(){
		URL resource = m_budleContext.getBundle().getResource("default_validation_queries");
		m_defaultValidationQueries = new Properties();
		try {
			m_defaultValidationQueries.load(resource.openStream());
		} catch (IOException e) {
			m_logService.log(LogService.LOG_WARNING, "Failed to load default validation queries", e);
		}
	}
	
	public String getValidationQuery(String driverClassName){
		return m_defaultValidationQueries.getProperty(driverClassName);
	}
	
}
