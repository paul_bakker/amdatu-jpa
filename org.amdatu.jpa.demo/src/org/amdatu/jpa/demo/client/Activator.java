/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.demo.client;

import java.util.Properties;

import org.amdatu.jpa.demo.DemoService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		Properties props = new Properties();
		
		props.put(CommandProcessor.COMMAND_SCOPE, "demo");
		props.put(CommandProcessor.COMMAND_FUNCTION, new String[] {"get", "list", "add", "delete"});
		
		
		dm.add(createComponent().setInterface(Object.class.getName(), props).setImplementation(Client.class).add(
				createServiceDependency().setService(DemoService.class).setRequired(true)));

	}

}
