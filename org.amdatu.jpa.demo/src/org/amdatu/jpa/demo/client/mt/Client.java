/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.demo.client.mt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.jpa.demo.DemoRecord;
import org.amdatu.jpa.demo.DemoService;
import org.osgi.framework.ServiceReference;

public class Client {

	private volatile Map<String, DemoService> m_demoServiceMap = new HashMap<>();

	public DemoRecord get(String pid, Long id) {
		return m_demoServiceMap.get(pid).get(id);
	}

	public List<DemoRecord> list(String pid) {
		return m_demoServiceMap.get(pid).list();
	}

	public void add(String pid, String text) {
		m_demoServiceMap.get(pid).add(text);
	}

	public void delete(String pid, Long id) {
		m_demoServiceMap.get(pid).delete(id);
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onAdded(ServiceReference<DemoService> ref, DemoService service) {
		String tenantPid = (String) ref.getProperty("org.amdatu.tenant.pid");
		synchronized (m_demoServiceMap) {
			m_demoServiceMap.put(tenantPid, service); 
		}
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onRemoved(ServiceReference<DemoService> ref, DemoService service) {
		String tenantPid = (String) ref.getProperty("org.amdatu.tenant.pid");
		synchronized (m_demoServiceMap) {
			m_demoServiceMap.remove(tenantPid);
		}
	}

}
