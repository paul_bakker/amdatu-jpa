/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.demo.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;

import org.amdatu.jpa.demo.DemoRecord;
import org.amdatu.jpa.demo.DemoService;
import org.amdatu.jta.Transactional;
import org.amdatu.jta.Transactional.TxType;

@Transactional
public class DemoServiceImpl implements DemoService {

	private volatile EntityManager entityManager;
	
	@Transactional(TxType.SUPPORTS)
	@Override
	public DemoRecord get(Long id) {
		JpaDemoRecord find = entityManager.find(JpaDemoRecord.class, id);
		return toDemoRecord(find);
	}

	private DemoRecord toDemoRecord(JpaDemoRecord find) {
		DemoRecord demoRecord = new DemoRecord();
		demoRecord.setId(find.getId());
		demoRecord.setText(find.getText());
		return demoRecord;
	}

	@Transactional(TxType.SUPPORTS)
	@Override
	public List<DemoRecord> list() {
		TypedQuery<JpaDemoRecord> query = entityManager.createQuery("SELECT dr FROM JpaDemoRecord dr", JpaDemoRecord.class);
		
		ArrayList<DemoRecord> arrayList = new ArrayList<>();
		for (JpaDemoRecord jpaDemoRecord: query.getResultList()) {
			arrayList.add(toDemoRecord(jpaDemoRecord));
		}
		
		return arrayList;
	}

	@Override
	public Long add(String text) {
		JpaDemoRecord jpaDemoRecord = new JpaDemoRecord();
		jpaDemoRecord.setText(text);
		entityManager.persist(jpaDemoRecord);
		return jpaDemoRecord.getId();
	}

	@Override
	public void delete(Long id) {
		JpaDemoRecord find = entityManager.find(JpaDemoRecord.class, id);
		if (find != null){
			entityManager.remove(find);
		}else{
			throw new EntityNotFoundException();
		}
	}
}
