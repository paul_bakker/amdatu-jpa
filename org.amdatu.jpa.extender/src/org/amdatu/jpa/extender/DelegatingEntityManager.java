/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.Metamodel;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionSynchronizationRegistry;

public class DelegatingEntityManager implements EntityManager {

	private volatile EntityManagerFactory entityManagerFactory;
	private volatile TransactionManager transactionManager;
	private volatile TransactionSynchronizationRegistry transactionSynchronizationRegistry;
	
	private EntityManager nonTxEntityManager;
	
	private EntityManager getEntityManager(){
		try {
			if (transactionManager.getStatus() == Status.STATUS_ACTIVE) {
				EntityManager txEntityManager = (EntityManager) this.transactionSynchronizationRegistry
						.getResource(entityManagerFactory);

				if (txEntityManager == null) {
					txEntityManager = entityManagerFactory.createEntityManager();
					txEntityManager.joinTransaction();

					this.transactionSynchronizationRegistry.putResource(entityManagerFactory, txEntityManager);

					try {
						transactionManager.getTransaction().registerSynchronization(
								new EntityManagerClosingSynchronization(txEntityManager));
					} catch (IllegalStateException | RollbackException e) {
						// This won't happen as the transaction state is known to be active
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}

				return txEntityManager;
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		if (nonTxEntityManager == null){
			nonTxEntityManager = entityManagerFactory.createEntityManager();
		}
		
		return nonTxEntityManager;
	}
	
	/**
	 * Transaction Synchronization that closes an EntityManager that is bound to this transaction
	 */
	class EntityManagerClosingSynchronization implements Synchronization {

		private EntityManager entityManager;

		public EntityManagerClosingSynchronization(EntityManager entityManager) {
			super();
			this.entityManager = entityManager;
		}

		@Override
		public void beforeCompletion() {

		}

		@Override
		public void afterCompletion(int arg0) {
			synchronized (DelegatingEntityManager.this) {
				if (nonTxEntityManager != null){
					nonTxEntityManager.clear();
				}
				entityManager.close();
			}
		}
	}

	public synchronized void clear() {
		getEntityManager().clear();
	}

	public synchronized void close() {
		//n00p
	}

	public synchronized boolean contains(Object arg0) {
		return getEntityManager().contains(arg0);
	}

	public synchronized Query createNamedQuery(String arg0) {
		return getEntityManager().createNamedQuery(arg0);
	}

	public synchronized <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
		return getEntityManager().createNamedQuery(arg0, arg1);
	}

	public synchronized Query createNativeQuery(String arg0) {
		return getEntityManager().createNativeQuery(arg0);
	}

	public synchronized Query createNativeQuery(String arg0, @SuppressWarnings("rawtypes") Class arg1) {
		return getEntityManager().createNativeQuery(arg0, arg1);
	}

	public synchronized Query createNativeQuery(String arg0, String arg1) {
		return getEntityManager().createNativeQuery(arg0, arg1);
	}

	public synchronized Query createQuery(String arg0) {
		return getEntityManager().createQuery(arg0);
	}

	public synchronized <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
		return getEntityManager().createQuery(arg0);
	}

	public synchronized <T> TypedQuery<T> createQuery(String arg0, Class<T> arg1) {
		return getEntityManager().createQuery(arg0, arg1);
	}

	public synchronized void detach(Object arg0) {
		getEntityManager().detach(arg0);
	}

	public synchronized <T> T find(Class<T> arg0, Object arg1) {
		return getEntityManager().find(arg0, arg1);
	}

	public synchronized <T> T find(Class<T> arg0, Object arg1, Map<String, Object> arg2) {
		return getEntityManager().find(arg0, arg1, arg2);
	}

	public synchronized <T> T find(Class<T> arg0, Object arg1, LockModeType arg2) {
		return getEntityManager().find(arg0, arg1, arg2);
	}

	public synchronized <T> T find(Class<T> arg0, Object arg1, LockModeType arg2, Map<String, Object> arg3) {
		return getEntityManager().find(arg0, arg1, arg2, arg3);
	}

	public synchronized void flush() {
		getEntityManager().flush();
	}

	public synchronized CriteriaBuilder getCriteriaBuilder() {
		return getEntityManager().getCriteriaBuilder();
	}

	public synchronized Object getDelegate() {
		return getEntityManager().getDelegate();
	}

	public synchronized EntityManagerFactory getEntityManagerFactory() {
		return getEntityManager().getEntityManagerFactory();
	}

	public synchronized FlushModeType getFlushMode() {
		return getEntityManager().getFlushMode();
	}

	public synchronized LockModeType getLockMode(Object arg0) {
		return getEntityManager().getLockMode(arg0);
	}

	public synchronized Metamodel getMetamodel() {
		return getEntityManager().getMetamodel();
	}

	public synchronized Map<String, Object> getProperties() {
		return getEntityManager().getProperties();
	}

	public synchronized <T> T getReference(Class<T> arg0, Object arg1) {
		return getEntityManager().getReference(arg0, arg1);
	}

	public synchronized EntityTransaction getTransaction() {
		return getEntityManager().getTransaction();
	}

	public synchronized boolean isOpen() {
		return getEntityManager().isOpen();
	}

	public synchronized void joinTransaction() {
		//n00p
	}

	public synchronized void lock(Object arg0, LockModeType arg1) {
		getEntityManager().lock(arg0, arg1);
	}

	public synchronized void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
		getEntityManager().lock(arg0, arg1, arg2);
	}

	public synchronized <T> T merge(T arg0) {
		return getEntityManager().merge(arg0);
	}

	public synchronized void persist(Object arg0) {
		getEntityManager().persist(arg0);
	}

	public synchronized void refresh(Object arg0) {
		getEntityManager().refresh(arg0);
	}

	public synchronized void refresh(Object arg0, Map<String, Object> arg1) {
		getEntityManager().refresh(arg0, arg1);
	}

	public synchronized void refresh(Object arg0, LockModeType arg1) {
		getEntityManager().refresh(arg0, arg1);
	}

	public synchronized void refresh(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
		getEntityManager().refresh(arg0, arg1, arg2);
	}

	public synchronized void remove(Object arg0) {
		getEntityManager().remove(arg0);
	}

	public synchronized void setFlushMode(FlushModeType arg0) {
		getEntityManager().setFlushMode(arg0);
	}

	public synchronized void setProperty(String arg0, Object arg1) {
		getEntityManager().setProperty(arg0, arg1);
	}

	public synchronized <T> T unwrap(Class<T> arg0) {
		return getEntityManager().unwrap(arg0);
	}
	
}
