/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import javax.persistence.spi.ClassTransformer;

import org.amdatu.jpa.adapter.PersistenceProviderAdapter;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.hooks.weaving.WeavingHook;
import org.osgi.framework.hooks.weaving.WovenClass;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.log.LogService;

public class EnhanceWeavingHook implements WeavingHook {

	private final PersistenceUnitInfoImpl m_persistenceUnitInfo;

	private volatile LogService logService;
	
	public EnhanceWeavingHook(PersistenceUnitInfoImpl persistenceUnitInfo) {
		this.m_persistenceUnitInfo = persistenceUnitInfo;
	}

	@Override 
	public void weave(final WovenClass wovenClass) {
		
		String clazzName = wovenClass.getClassName();
		
		if (clazzName.contains("$")){
			clazzName = clazzName.split("\\$")[0];
		}
		
		if (m_persistenceUnitInfo.getManagedClassNames().contains(clazzName)) {
			try {
				Bundle persistenceProviderBundle = m_persistenceUnitInfo.getPersistenceProviderBundle();
				 
				
				if (m_persistenceUnitInfo.getPersistenceProvider() instanceof PersistenceProviderAdapter){
					wovenClass.getDynamicImports().add("*");
				}else{
					String suffix = ";" + Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE + "="
							+ persistenceProviderBundle.getSymbolicName() + ";"
							+ Constants.BUNDLE_VERSION_ATTRIBUTE + "=" + persistenceProviderBundle.getVersion();
					
					BundleRevision br = persistenceProviderBundle.adapt(BundleWiring.class).getRevision();
					
					
					for (BundleCapability bc : br.getDeclaredCapabilities(BundleRevision.PACKAGE_NAMESPACE)) {
						String importToAdd = bc.getAttributes().get(BundleRevision.PACKAGE_NAMESPACE) + suffix;
						wovenClass.getDynamicImports().add(importToAdd);
					}
				}
			
				for (ClassTransformer transformer : m_persistenceUnitInfo.getClassTransformers()) {
					final byte[] transform = transformer.transform(wovenClass.getBundleWiring().getClassLoader(),
							clazzName, wovenClass.getDefinedClass(),
							wovenClass.getProtectionDomain(), wovenClass.getBytes());
					
					if (transform != null) {
						wovenClass.setBytes(transform);
					}
				}
			} catch (Exception e) {
				logService.log(LogService.LOG_ERROR, "Weaving failed for: " + wovenClass.getClassName(), e);
			}
		}
	}
}
