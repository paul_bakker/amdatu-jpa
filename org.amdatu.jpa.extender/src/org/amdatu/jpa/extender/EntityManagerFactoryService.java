/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;

public class EntityManagerFactoryService implements EntityManagerFactory {

	private final PersistenceUnitInfoImpl m_persistenceUnitInfo;
	private EntityManagerFactory m_delegate;

	public EntityManagerFactoryService(PersistenceUnitInfoImpl persistenceUnitInfoImpl) {
		this.m_persistenceUnitInfo = persistenceUnitInfoImpl;
	}

	@Override
	public void close() {
		//n00p
	}

	@Override
	public EntityManager createEntityManager() {
		return m_delegate.createEntityManager();
	}

	@Override
	public EntityManager createEntityManager(@SuppressWarnings("rawtypes") Map arg0) {
		throw new RuntimeException("not supported");
	}

	@Override
	public Cache getCache() {
		return m_delegate.getCache();
	}

	@Override
	public CriteriaBuilder getCriteriaBuilder() {
		return m_delegate.getCriteriaBuilder();
	}

	@Override
	public Metamodel getMetamodel() {
		return m_delegate.getMetamodel();
	}

	@Override
	public PersistenceUnitUtil getPersistenceUnitUtil() {
		return m_delegate.getPersistenceUnitUtil();
	}

	@Override
	public Map<String, Object> getProperties() {
		return m_delegate.getProperties();
	}

	@Override
	public boolean isOpen() {
		return m_delegate.isOpen();
	}
	
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void start(){
		m_delegate = m_persistenceUnitInfo.getPersistenceProvider().createContainerEntityManagerFactory(m_persistenceUnitInfo, new HashMap<>());
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void stop(){
		m_delegate.close();
	}
	
}
