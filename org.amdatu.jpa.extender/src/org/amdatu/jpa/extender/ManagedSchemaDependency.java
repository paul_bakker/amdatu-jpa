/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Properties;

import javax.persistence.spi.PersistenceUnitInfo;
import javax.sql.DataSource;

import org.amdatu.database.schemamigration.SchemaMigrationException;
import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.Dependency;
import org.apache.felix.dm.DependencyActivation;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.DependencyService;
import org.osgi.framework.Bundle;

/**
 * Dependency Manager dependency type that is added to an EntityManager / EntityManagerFactory to make sure 
 * schema migrations are performed before the EntityManager(Factory) service becomes available. 
 * 
 *   This dependency uses properties from the PersistenceUnitInfo ( persistence.xml ) to perform schema migrations using 
 *   a {@link SchemaMigrationService}.
 *   
 *   amdatu.jpa.managed_schema - true -> Schema management enabled 
 *   amdatu.jpa.managed_schema.migration_service_filter - OSGi used to retrieve filter for the {@link SchemaMigrationService}
 *   amdatu.jpa.managed_schema.locations - Location in the persistence bundle where the migration files can be found
 * 	 amdatu.jpa.managed_schema.name - Name of the schema (when using multiple schema's in one database)   
 *
 */
public class ManagedSchemaDependency implements Dependency, DependencyActivation {

	
	private volatile SchemaMigrationService m_schemaMigrationService;
	
	private volatile String m_schemaVersion;
	
	private final DependencyManager m_dependencyManager;
	private final PersistenceUnitInfo m_puInfo;
	private final Bundle m_persisteneBundle;
	
    private final List<DependencyService> m_services = new ArrayList<>();

	private final Component m_component;
	
	public ManagedSchemaDependency(DependencyManager dm, PersistenceUnitInfo puInfo, Bundle persistenceBundle) {
		this.m_dependencyManager = dm;
		this.m_puInfo = puInfo;
		this.m_persisteneBundle = persistenceBundle;
		
		
		String migrationServiceFilter = (String) puInfo.getProperties().get("amdatu.jpa.managed_schema.migration_service_filter");
		
		m_component = dm.createComponent().setImplementation(this);
		String isManagedSchema = (String) puInfo.getProperties().get("amdatu.jpa.managed_schema");
		if (isManagedSchema != null && Boolean.valueOf(isManagedSchema)){
				m_component.add(dm.createServiceDependency().setService(SchemaMigrationService.class, migrationServiceFilter).setRequired(true));
		}
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void start(){
		String isManagedSchema = (String) m_puInfo.getProperties().get("amdatu.jpa.managed_schema");
		if (!(isManagedSchema != null && Boolean.valueOf(isManagedSchema))){
			m_schemaVersion = "Not managed";
			return;
		}

		String migrationScriptDir = (String) m_puInfo.getProperties().get("amdatu.jpa.managed_schema.locations");
		if (migrationScriptDir == null){
			throw new IllegalStateException("Migration script dir should not be null");
		}
		
		if (m_persisteneBundle == null){
			throw new IllegalStateException("Persistence bundle should not be null");
		}
		
		String schemaName = (String) m_puInfo.getProperties().get("amdatu.jpa.managed_schema.name");
		
		DataSource dataSource = m_puInfo.getNonJtaDataSource();
		if (dataSource == null){
			dataSource = m_puInfo.getJtaDataSource();
		}
		
		if (dataSource == null){
			throw new IllegalStateException("No datasource available for schema migration");
		}		
		
		try {
			m_schemaVersion = m_schemaMigrationService.migrate(dataSource, m_persisteneBundle, schemaName, migrationScriptDir);
			
			for (DependencyService service : m_services) {
				service.dependencyAvailable(this);
			}
		}catch (SchemaMigrationException e){
			e.printStackTrace();
		}
	}

	@Override
	public Dependency createCopy() {
		throw new IllegalStateException("Not supported");
	}

	@Override
	public Object getAutoConfigInstance() {
		return null;
	}

	@Override
	public String getAutoConfigName() {
		return null;
	}

	@Override
	public Class<?> getAutoConfigType() {
		return null;
	}

	@Override
	public Dictionary<?,?> getProperties() {
		Properties properties = new Properties();
		properties.put("version", "Not managed");
		return properties;
	}

	@Override
	public void invokeAdded(DependencyService dependencyService) {
		invoke(dependencyService, "added");
	}

	@Override
	public void invokeRemoved(DependencyService dependencyService) {
		invoke(dependencyService, "removed");
	}
	
    public void invoke(DependencyService dependencyService, String name) {
        if (name != null) {
            dependencyService.invokeCallbackMethod(getCallbackInstances(dependencyService), name,
              new Class[][] {{String.class}, {Object.class}, {}},
              new Object[][] {{getAutoConfigInstance()}, {getAutoConfigInstance()}, {}}
            );
        }
    }
    
    private synchronized Object[] getCallbackInstances(DependencyService dependencyService) {
        return dependencyService.getCompositionInstances();
    }

	@Override
	public boolean isAutoConfig() {
		return false;
	}

	@Override
	public boolean isAvailable() {
		return m_schemaVersion != null;
	}

	@Override
	public boolean isInstanceBound() {
		return false;
	}

	@Override
	public boolean isPropagated() {
		return false;
	}

	@Override
	public boolean isRequired() {
		return true;
	}

    public void start(DependencyService service) {
        synchronized (m_services) {
            m_services.add(service);
            
            if (m_services.size() == 1){
            	m_dependencyManager.add(m_component);	
            }
            
            if (m_schemaVersion != null){
            	service.dependencyAvailable(this);
            }
        }
    }

    public void stop(DependencyService service) {
        synchronized (m_services) {
            m_services.remove(service);
            if (m_services.isEmpty()){
            	m_dependencyManager.remove(m_component);
            } 
        }
    }
    
    @Override
    public String toString() {
    	return "org.amdatu.jpa.extender.ManagedSchemaDependency [version: " +  m_schemaVersion + "] "; 	
    }
}
