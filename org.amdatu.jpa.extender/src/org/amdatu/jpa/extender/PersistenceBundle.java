/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_NAME;
import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_PROVIDER;
import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_VERSION;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionSynchronizationRegistry;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.ComponentStateListener;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.hooks.weaving.WeavingHook;
import org.osgi.service.log.LogService;

public class PersistenceBundle {

	private static final String DEFAULT_PERSISTENCE_XML_RESOURCE = "META-INF/persistence.xml";
	private static final String META_PERSISTENCE_HEADER = "Meta-Persistence";

	private volatile LogService m_log;

	private volatile DependencyManager dependencyManager;
	
	private final Bundle bundle;

	private final List<PersistenceDescriptor> persistenceDescriptors;

	private final List<Component> puInfoComponents = new ArrayList<>();

	private final List<Component> persistenceBundleJpaComponents = new ArrayList<>();

	public PersistenceBundle(Bundle bundle) {
		this.bundle = bundle;
		this.persistenceDescriptors = readPersistenceDescriptors();
	}
	
	private List<PersistenceDescriptor> readPersistenceDescriptors() {
		PersistenceXmlParser persistenceXmlParser = new PersistenceXmlParser();

		String metaPersistenceHeader = bundle.getHeaders().get(META_PERSISTENCE_HEADER);
		List<String> persistenceXmlFiles = new ArrayList<>(); 
		persistenceXmlFiles.addAll(Arrays.asList(metaPersistenceHeader.split(",")));
		if (!!!persistenceXmlFiles.contains(DEFAULT_PERSISTENCE_XML_RESOURCE)) {
			// OSGi JPA specification states that "META-INF/persistence.xml" is
			// always used even if its not in the Meta-Persistence header
			persistenceXmlFiles.add(DEFAULT_PERSISTENCE_XML_RESOURCE);
		}

		List<PersistenceDescriptor> descriptors = new ArrayList<>();
		for (String persistenceXmlFile : persistenceXmlFiles) {
			URL resource = bundle.getResource(persistenceXmlFile);
			if (resource != null) {
				try (InputStream inputStream = resource.openStream()) {
					descriptors.addAll(persistenceXmlParser.parse(inputStream));
				} catch (IOException e) {
					m_log.log(LOG_ERROR,
							String.format("Unable to read persistence descriptor [%s].", persistenceXmlFile, e));
				}
			} else if (persistenceXmlFile.equals(DEFAULT_PERSISTENCE_XML_RESOURCE) && persistenceXmlFiles.size() > 1) {
				m_log.log(LOG_DEBUG, String.format("Default persistence descriptor [%s] not found.",
						DEFAULT_PERSISTENCE_XML_RESOURCE));
			} else {
				m_log.log(LOG_WARNING, String.format("Persistence descriptor [%s] not found.", persistenceXmlFile));
			}
		}
		return descriptors;
	}

	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void init() {
		final DependencyManager persistenceBundleDm = new DependencyManager(bundle.getBundleContext());
		
		for (PersistenceDescriptor persistenceDescriptor : persistenceDescriptors) {
			try {
				final PersistenceUnitInfoImpl puInfo = new PersistenceUnitInfoImpl(bundle, persistenceDescriptor);

				Component puInfoComponent = dependencyManager.createComponent().setImplementation(puInfo);
				
				String filter = null;
				String provider = persistenceDescriptor.getProvider();
				if (provider != null && provider.trim().length() > 0) {
					filter = String.format("(javax.persistence.provider=%s)", provider);
				}
				
				puInfoComponent.add(persistenceBundleDm.createServiceDependency().setService(PersistenceProvider.class, filter)
						.setCallbacks("addPersistenceProvider", "removePersistenceProvider")
						.setRequired(true));
				 

				if (puInfo.getPersistenceDescriptor().getNonJtaDataSource() != null) {
					String nonJtaDataSource = puInfo.getPersistenceDescriptor().getNonJtaDataSource();
					String dataSourceFilter = null;
					if (nonJtaDataSource.split("/").length > 1) {
						dataSourceFilter = nonJtaDataSource.split("/")[nonJtaDataSource.split("/").length - 1];
					}

					puInfoComponent.add(persistenceBundleDm.createServiceDependency()
							.setService(DataSource.class, dataSourceFilter)
							.setCallbacks("setNonJtaDataSource", "removeNonJtaDataSource")
							.setRequired(true));
				}

				if (puInfo.getPersistenceDescriptor().getJtaDataSource() != null) {
					String jtaDataSource = puInfo.getPersistenceDescriptor().getJtaDataSource();
					String jtaDataSourceFilter = null;
					if (jtaDataSource.split("/").length > 1) {
						jtaDataSourceFilter = jtaDataSource.split("/")[jtaDataSource.split("/").length - 1];
					}

					puInfoComponent
							.add(persistenceBundleDm.createServiceDependency()
									.setService(DataSource.class, jtaDataSourceFilter)
									.setCallbacks("setJtaDataSource", "removeJtaDataSource")
									.setRequired(true));
				}

				
				puInfoComponent.addStateListener(new ComponentStateListener() {
					
					private Component m_weavingHookComponent;
					private Component m_entityManagerFactoryServiceComponent;
					private Component m_entityManagerComponent; 
					
					@Override
					public void stopping(Component arg0) {
						persistenceBundleDm.remove(m_weavingHookComponent);
						persistenceBundleDm.remove(m_entityManagerFactoryServiceComponent);
						persistenceBundleDm.remove(m_entityManagerComponent);
						
					}
					
					@Override
					public void stopped(Component arg0) {
						
					}
					
					@Override
					public void starting(Component arg0) {
						
					}
					
					@Override
					public void started(Component arg0) {
						
						EnhanceWeavingHook enhanceWeavingHook = new EnhanceWeavingHook(puInfo);

						Properties properties = new Properties();
						properties.put("pb", bundle.getSymbolicName());
						properties.put("pu", puInfo.getPersistenceUnitName());
						
						m_weavingHookComponent = persistenceBundleDm
								.createComponent()
								.setInterface(WeavingHook.class.getName(), properties)
								.setImplementation(enhanceWeavingHook)
								.add(dependencyManager.createServiceDependency().setService(LogService.class)
										.setRequired(false));
						persistenceBundleDm.add(m_weavingHookComponent);
						
						
						EntityManagerFactoryService entityManagerFactoryService = new EntityManagerFactoryService(puInfo);

						Properties props = new Properties();

						props.put(JPA_UNIT_NAME, puInfo.getPersistenceUnitName());
						props.put(JPA_UNIT_PROVIDER, puInfo.getPersistenceProvider().getClass().getName());
						props.put(JPA_UNIT_VERSION, bundle.getVersion());


						m_entityManagerFactoryServiceComponent = persistenceBundleDm
								.createComponent()
								.setInterface(EntityManagerFactory.class.getName(), props)
								.setImplementation(entityManagerFactoryService)
								.add(persistenceBundleDm.createServiceDependency().setService(TransactionManager.class)
										.setRequired(true))
								.add(persistenceBundleDm.createServiceDependency()
										.setService(WeavingHook.class, String.format("(pb=%s)", bundle.getSymbolicName()))
										.setRequired(true));

						if (puInfo.getPersistenceDescriptor().getNonJtaDataSource() != null) {
							String nonJtaDataSource = puInfo.getPersistenceDescriptor().getNonJtaDataSource();
							String dataSourceFilter = null;
							if (nonJtaDataSource.split("/").length > 1) {
								dataSourceFilter = nonJtaDataSource.split("/")[nonJtaDataSource.split("/").length - 1];
							}

							m_entityManagerFactoryServiceComponent.add(persistenceBundleDm.createServiceDependency()
									.setService(DataSource.class, dataSourceFilter)
									.setRequired(true));
						}

						if (puInfo.getPersistenceDescriptor().getJtaDataSource() != null) {
							String jtaDataSource = puInfo.getPersistenceDescriptor().getJtaDataSource();
							String jtaDataSourceFilter = null;
							if (jtaDataSource.split("/").length > 1) {
								jtaDataSourceFilter = jtaDataSource.split("/")[jtaDataSource.split("/").length - 1];
							}

							m_entityManagerFactoryServiceComponent.add(persistenceBundleDm.createServiceDependency()
									.setService(DataSource.class, jtaDataSourceFilter)
									.setRequired(true));
						}
						
						m_entityManagerFactoryServiceComponent.add(persistenceBundleDm.createServiceDependency().setService(WeavingHook.class, 
								String.format("(&(pb=%s)(pu=%s))", bundle.getSymbolicName(), puInfo.getPersistenceUnitName())));
						
						ManagedSchemaDependency managedSchemaDependency = new ManagedSchemaDependency(persistenceBundleDm, puInfo, bundle);
						m_entityManagerFactoryServiceComponent.add(managedSchemaDependency);
						
						persistenceBundleJpaComponents.add(m_entityManagerFactoryServiceComponent);
						persistenceBundleDm.add(m_entityManagerFactoryServiceComponent);

						String filter = String.format("(%s=%s)", JPA_UNIT_NAME, puInfo.getPersistenceUnitName());
						m_entityManagerComponent = persistenceBundleDm
								.createComponent()
								.setInterface(EntityManager.class.getName(), props)
								.setImplementation(new DelegatingEntityManager())
								.add(persistenceBundleDm.createServiceDependency()
										.setService(EntityManagerFactory.class, filter).setRequired(true))
								.add(persistenceBundleDm.createServiceDependency().setService(TransactionManager.class)
										.setRequired(true))
								.add(persistenceBundleDm.createServiceDependency()
										.setService(TransactionSynchronizationRegistry.class).setRequired(true))
								.add(persistenceBundleDm.createServiceDependency().setService(WeavingHook.class, 
										String.format("(&(pb=%s)(pu=%s))", bundle.getSymbolicName(), puInfo.getPersistenceUnitName()))
										.setRequired(true));
						
						m_entityManagerComponent.add(managedSchemaDependency);
						
						persistenceBundleJpaComponents.add(m_entityManagerComponent);
						persistenceBundleDm.add(m_entityManagerComponent);
						
					}
				});
				
				puInfoComponents.add(puInfoComponent);
				dependencyManager.add(puInfoComponent);

				
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void stop() {
		for (Component component : puInfoComponents) {
			dependencyManager.remove(component);
		}
	}
	
}
