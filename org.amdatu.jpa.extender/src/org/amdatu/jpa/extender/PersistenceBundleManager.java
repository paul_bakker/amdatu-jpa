/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.service.log.LogService;

public class PersistenceBundleManager {

	private volatile LogService m_log;

	private volatile DependencyManager dependencyManager;

	private volatile Map<Bundle, Component> persistenceBundleComponents = new ConcurrentHashMap<>();

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void addPersistenceBundle(Bundle bundle) {
		synchronized (persistenceBundleComponents) {

			if (persistenceBundleComponents.containsKey(bundle)) {
				m_log.log(
						LOG_WARNING,
						String.format("Trying to add registered bundle [%s] %s", bundle.getBundleId(),
								bundle.getSymbolicName()));
				return;
			}

			m_log.log(LOG_INFO, "Adding persistence bundle: " + bundle);

			try {
				PersistenceBundle persistenceBundle = new PersistenceBundle(bundle);

				Properties props = new Properties();
				props.put("bundle", bundle.getBundleId());

				Component component = dependencyManager
						.createComponent()
						.setInterface(Object.class.getName(), props)
						.setImplementation(persistenceBundle)
						.add(dependencyManager.createServiceDependency().setService(LogService.class))
						.add(dependencyManager.createServiceDependency().setService(PersistenceBundleManager.class)
								.setRequired(true));

				dependencyManager.add(component);
				persistenceBundleComponents.put(bundle, component);
				m_log.log(LOG_INFO, "New persistence bundle added: " + bundle);
			} catch (Throwable e) {
				m_log.log(LOG_ERROR, "Failed to register persistence bundle", e);
			}
		}

	}

	public void removePersistenceBundle(Bundle bundle) {
		m_log.log(LOG_INFO, "Removing persistence bundle: " + bundle);
		synchronized (persistenceBundleComponents) {
			if (persistenceBundleComponents.containsKey(bundle)) {
				dependencyManager.remove(persistenceBundleComponents.remove(bundle));
			}
		}
	}
	
}
