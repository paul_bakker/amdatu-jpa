/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PersistenceDescriptor {

	public enum PersistenceXmlElements {
		
		PROVIDER("provider"),
		JTA_DATASOURCE("jta-data-source"),
		NON_JTA_DATASOURCE("non-jta-data-source"), 
		CLASS("class");
		
		String elementName;
		
		PersistenceXmlElements(String elementName){
			this.elementName = elementName;
		}
		
		public static PersistenceXmlElements getElement(String name){
			for (PersistenceXmlElements e: PersistenceXmlElements.values()){
				if (e.elementName.equals(name)){
					return e;
				}
			}
			return null;
		}
		
	}; 
	
	private String unitName; 
	
	private String jtaDataSource; 
	
	private String nonJtaDataSource; 
	
	private String provider; 
	
	private List<String> classes = new ArrayList<>(); 
	
	private boolean excludeUnlistedClasses;
	
	private Properties properties = new Properties();

	public PersistenceDescriptor(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getJtaDataSource() {
		return jtaDataSource;
	}

	public void setJtaDataSource(String jtaDataSource) {
		this.jtaDataSource = jtaDataSource;
	}

	public String getNonJtaDataSource() {
		return nonJtaDataSource;
	}

	public void setNonJtaDataSource(String nonJtaDataSource) {
		this.nonJtaDataSource = nonJtaDataSource;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public List<String> getClasses() {
		return classes;
	}

	public void addClass(String className) {
		this.classes.add(className);
	} 

	public boolean isExcludeUnlistedClasses() {
		return excludeUnlistedClasses;
	}

	public void setExcludeUnlistedClasses(boolean excludeUnlistedClasses) {
		this.excludeUnlistedClasses = excludeUnlistedClasses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unitName == null) ? 0 : unitName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersistenceDescriptor other = (PersistenceDescriptor) obj;
		if (unitName == null) {
			if (other.unitName != null)
				return false;
		} else if (!unitName.equals(other.unitName))
			return false;
		return true;
	}

	public void setProperty(String value, String value2) {
		properties.put(value, value2);
	}
	
	public Properties getProperties() {
		return properties;
	}
}
