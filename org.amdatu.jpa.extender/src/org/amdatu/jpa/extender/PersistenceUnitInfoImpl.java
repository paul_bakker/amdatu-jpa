/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.wiring.BundleWiring;

public class PersistenceUnitInfoImpl implements PersistenceUnitInfo {

	private volatile List<ClassTransformer> classTransformers = new ArrayList<>();
	private final Bundle bundle;
	private final PersistenceDescriptor persistenceDescriptor;

	private volatile DataSource nonJtaDataSource;
	private volatile DataSource jtaDataSource;
	private volatile PersistenceProvider persistenceProvider;
	private volatile ServiceReference<PersistenceProvider> persistenceProviderRef;

	public PersistenceUnitInfoImpl(Bundle bundle, PersistenceDescriptor persistenceDescriptor) {
		this.bundle = bundle;
		this.persistenceDescriptor = persistenceDescriptor;
	}

	@Override
	public void addTransformer(ClassTransformer classTransformer) {
		classTransformers.add(classTransformer);
	}

	@Override
	public boolean excludeUnlistedClasses() {
		return true;
	}

	@Override
	public ClassLoader getClassLoader() {
		return bundle.adapt(BundleWiring.class).getClassLoader();
	}

	@Override
	public List<URL> getJarFileUrls() {
		return Collections.emptyList();
	}

	@Override
	public DataSource getJtaDataSource() {
		return jtaDataSource;
	}

	@Override
	public List<String> getManagedClassNames() {
		return persistenceDescriptor.getClasses();
	}

	@Override
	public List<String> getMappingFileNames() {
		return Collections.emptyList();
	}

	@Override
	public ClassLoader getNewTempClassLoader() {

		return new ClassLoader() {

			@SuppressWarnings("rawtypes")
			private Map<String, Class> loaded = new HashMap<>(); 
			
			@Override
			public Class<?> loadClass(String name) throws ClassNotFoundException {
				if (loaded.containsKey(loaded.containsKey(name))){
					return loaded.get(name);
				}
				
				String clazzName = name;
				if (name.contains("$")){
					clazzName = name.split("\\$")[0];
				}
				
				if (!!!persistenceDescriptor.getClasses().contains(clazzName)) {
					/*
					 * Delegate class loading to the bundle ClassLoader in case
					 * a the class that is loaded is not an Entity class
					 */
					return getClassLoader().loadClass(name);
				}
				
				Class<?> clazz = findClass(name);
				loaded.put(name, clazz);
				return clazz;
			}

			@Override
			protected Class<?> findClass(String className) throws ClassNotFoundException {
				String classResName = className.replace('.', '/').concat(".class");

				InputStream is = null;
				URL url = bundle.getResource(classResName);
				try {
					is = (url == null) ? null : url.openStream();
				} catch (IOException e) {

				}

				if (is == null) {
					throw new ClassNotFoundException(className);
				}

				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				byte[] buff = new byte[4096];
				try {
					try {
						int read = is.read(buff);
						while (read > 0) {
							baos.write(buff, 0, read);
							read = is.read(buff);
						}
					} finally {
						is.close();
					}
				} catch (IOException ioe) {
					throw new ClassNotFoundException(className, ioe);
				}

				buff = baos.toByteArray();
				Class<?> defineClass = defineClass(className, buff, 0, buff.length);
				return defineClass;
			}
		};
	}

	@Override
	public DataSource getNonJtaDataSource() {
		return nonJtaDataSource;
	}

	@Override
	public String getPersistenceProviderClassName() {
		return persistenceDescriptor.getProvider();
	}

	@Override
	public String getPersistenceUnitName() {
		return persistenceDescriptor.getUnitName();
	}

	@Override
	public URL getPersistenceUnitRootUrl() {
		URL resource = bundle.getResource("/");
		return resource;
	}

	@Override
	public String getPersistenceXMLSchemaVersion() {
		return "2.0";
	}

	@Override
	public Properties getProperties() {
		return persistenceDescriptor.getProperties();
	}

	@Override
	public SharedCacheMode getSharedCacheMode() {
		// TODO
		return SharedCacheMode.UNSPECIFIED;
	}

	@Override
	public PersistenceUnitTransactionType getTransactionType() {
		return PersistenceUnitTransactionType.JTA;
	}

	@Override
	public ValidationMode getValidationMode() {
		// TODO
		return ValidationMode.NONE;
	}

	public List<ClassTransformer> getClassTransformers() {
		return classTransformers;
	}

	public PersistenceDescriptor getPersistenceDescriptor() {
		return persistenceDescriptor;
	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void setNonJtaDataSource(DataSource dataSource){
		this.nonJtaDataSource = dataSource;
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void removeNonJtaDataSource(DataSource dataSource){
		this.nonJtaDataSource = null;
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void setJtaDataSource(DataSource dataSource){
		this.jtaDataSource = dataSource;
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void removeJtaDataSource(DataSource dataSource){
		this.jtaDataSource = null;
	}

	public void addPersistenceProvider(ServiceReference<PersistenceProvider> ref,
			PersistenceProvider persistenceProvider) {
		this.persistenceProvider = persistenceProvider;
		this.persistenceProviderRef = ref;
	}

	public void removePersistenceProvider(ServiceReference<PersistenceProvider> ref) {
		this.persistenceProviderRef = null;
	}

	public Bundle getPersistenceProviderBundle() {
		return persistenceProviderRef.getBundle();
	}

	public PersistenceProvider getPersistenceProvider() {
		return persistenceProvider;
	}
	
}
