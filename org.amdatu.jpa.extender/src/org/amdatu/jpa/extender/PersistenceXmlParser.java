/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Persistence;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.SchemaFactory;

import org.amdatu.jpa.extender.PersistenceDescriptor.PersistenceXmlElements;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.Version;
import org.osgi.framework.namespace.PackageNamespace;
import org.osgi.framework.wiring.BundleCapability;
import org.osgi.framework.wiring.BundleRevision;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PersistenceXmlParser {

	private List<PersistenceDescriptor> persistenceDescriptors = new ArrayList<>();
	
	private final class PersistenceDescriptorHandler extends DefaultHandler {

		private StringBuilder builder = new StringBuilder();

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			builder.append(ch, start, length);
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if ("persistence-unit".equals(qName)) {
				String unitName = attributes.getValue("name");
				persistenceDescriptors.add(new PersistenceDescriptor(unitName));
			}else if ("property".equals(qName)){
				PersistenceDescriptor persistenceUnitDefinition = persistenceDescriptors.get(persistenceDescriptors.size() - 1);
				persistenceUnitDefinition.setProperty(attributes.getValue("name"), attributes.getValue("value"));
			} 
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {

			String elementValue = builder.toString().trim();
			builder = new StringBuilder();

			PersistenceXmlElements element = PersistenceXmlElements.getElement(qName);

			if (null != element) {
				PersistenceDescriptor persistenceUnitDefinition = persistenceDescriptors
						.get(persistenceDescriptors.size() - 1);
				switch (element) {
				case PROVIDER:
					persistenceUnitDefinition.setProvider(elementValue);
					break;
				case JTA_DATASOURCE:
					persistenceUnitDefinition.setJtaDataSource(elementValue);
					break;
				case NON_JTA_DATASOURCE:
					persistenceUnitDefinition.setNonJtaDataSource(elementValue);
					break;
				case CLASS:
					persistenceUnitDefinition.addClass(elementValue);
					break;
				}
			}
		}
	}

	public List<PersistenceDescriptor> parse(InputStream inputStream) {
		try {
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

			SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

			Bundle javaxPersistenceBundle = FrameworkUtil.getBundle(Persistence.class);
			BundleRevision adapt = javaxPersistenceBundle.adapt(BundleRevision.class);
			List<BundleCapability> declaredCapabilities = adapt.getDeclaredCapabilities(PackageNamespace.PACKAGE_NAMESPACE);
			Version version = null ;
			for (BundleCapability bc: declaredCapabilities){
				if ("javax.persistence".equals(bc.getAttributes().get(PackageNamespace.PACKAGE_NAMESPACE))){
					version = (Version) bc.getAttributes().get(PackageNamespace.CAPABILITY_VERSION_ATTRIBUTE);
				}
			}
			
			Bundle bundle = FrameworkUtil.getBundle(getClass());
			URL schemaUrl = bundle.getResource(String.format("xsd/persistence_%s_%s.xsd", version.getMajor(), 
					version.getMinor()));
			
			if (schemaUrl == null){
				throw new IllegalStateException(String.format("Unsupported javax.persistenve version %s.%s", 
						version.getMajor(), version.getMinor()));
			}
			
			saxParserFactory.setSchema(schemaFactory.newSchema(schemaUrl));

			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.parse(inputStream, new PersistenceDescriptorHandler());

			return persistenceDescriptors;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
