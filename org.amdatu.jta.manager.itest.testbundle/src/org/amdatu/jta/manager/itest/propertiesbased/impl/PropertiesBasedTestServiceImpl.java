/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager.itest.propertiesbased.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.amdatu.jta.Transactional;
import org.amdatu.jta.manager.itest.testbundle.PropertiesBasedTestService;
import org.amdatu.jta.manager.itest.testbundle.TestEntity;

@Transactional
public class PropertiesBasedTestServiceImpl implements PropertiesBasedTestService {

	private volatile EntityManager em;
	
	@Override
	public void save(TestEntity testEntity) {
		em.persist(testEntity);
	}

	@Override
	public List<TestEntity> list() {
		return em.createQuery("select t from TestEntity t", TestEntity.class).getResultList();
	}

	@Override
	public void clearDb() {
		em.createQuery("delete from TestEntity t").executeUpdate();
	}
}
