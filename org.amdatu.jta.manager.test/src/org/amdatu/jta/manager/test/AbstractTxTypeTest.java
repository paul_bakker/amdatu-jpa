/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager.test;

import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.util.tracker.ServiceTracker;

public class AbstractTxTypeTest {

	protected final BundleContext context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
	protected ServiceTracker<TestService, TestService> tracker;

	public AbstractTxTypeTest() {
		super();
	}

	@Before
	public void before() throws Exception {
		tracker = new ServiceTracker<TestService, TestService>(context, context.createFilter("(isManaged=true)"), null);
		tracker.open();
	}

	@After
	public void after() {
		tracker.close();
	}


	protected ServiceRegistration<Object> registerTestService(TestService testService) {
		return context.registerService(Object.class, testService, createProperties(TestService.class));
	}

	protected Hashtable<String, Object> createProperties(Class<TestService> class1) {
		Hashtable<String, Object> properties = new Hashtable<>();
		properties.put("transactional", TestService.class.getName());
		return properties;
	}
}