/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager.test;

import java.io.IOException;

import javax.transaction.Status;
import javax.transaction.TransactionManager;

import org.amdatu.jta.Transactional;
import org.amdatu.jta.Transactional.TxType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.ServiceRegistration;

public class TxTypeRequiredTest extends AbstractTxTypeTest {

	private class TestServiceImpl implements TestService {
		@Override
		@Transactional(TxType.REQUIRED)
		public void test() throws IOException {

		}
	}

	private class RuntimeBrokenTestServiceImpl implements TestService {
		@Override
		@Transactional(TxType.REQUIRED)
		public void test() throws IOException {
			throw new RuntimeException();
		}
	}

	private class CheckedBrokenTestServiceImpl implements TestService {
		@Override
		@Transactional(TxType.REQUIRED)
		public void test() throws IOException {
			throw new IOException();
		}
	}

	@Test
	public void testNoTransactionActive() throws Exception {

		TransactionManager mock = Mockito.mock(TransactionManager.class);
		Mockito.when(mock.getStatus()).thenReturn(Status.STATUS_UNKNOWN).thenReturn(Status.STATUS_ACTIVE);
		ServiceRegistration<TransactionManager> txManagerServiceReg = context.registerService(TransactionManager.class,
				mock, null);

		TestService testService = new TestServiceImpl();
		ServiceRegistration<Object> testServiceReg = registerTestService(testService);

		TestService service = tracker.waitForService(1000l);
		Assert.assertNotNull(service);
		service.test();

		txManagerServiceReg.unregister();
		testServiceReg.unregister();

		Mockito.verify(mock, Mockito.times(1)).begin();
		Mockito.verify(mock, Mockito.times(0)).setRollbackOnly();
		Mockito.verify(mock, Mockito.times(1)).commit();
	}

	@Test
	public void testTransactionActive() throws Exception {
		TransactionManager mock = Mockito.mock(TransactionManager.class);
		Mockito.when(mock.getStatus()).thenReturn(Status.STATUS_ACTIVE);
		ServiceRegistration<TransactionManager> txManagerServiceReg = context.registerService(TransactionManager.class,
				mock, null);

		TestService testService = new TestServiceImpl();
		ServiceRegistration<Object> testServiceReg = registerTestService(testService);

		TestService service = tracker.waitForService(1000l);
		Assert.assertNotNull(service);
		service.test();

		txManagerServiceReg.unregister();
		testServiceReg.unregister();

		Mockito.verify(mock, Mockito.times(0)).begin();
		Mockito.verify(mock, Mockito.times(0)).setRollbackOnly();
		Mockito.verify(mock, Mockito.times(0)).commit();
	}

	@Test(expected = RuntimeException.class)
	public void testThrowsRuntimeException() throws Exception {

		TransactionManager mock = Mockito.mock(TransactionManager.class);
		Mockito.when(mock.getStatus()).thenReturn(Status.STATUS_UNKNOWN).thenReturn(Status.STATUS_ACTIVE);
		ServiceRegistration<TransactionManager> txManagerServiceReg = context.registerService(TransactionManager.class,
				mock, null);

		TestService testService = new RuntimeBrokenTestServiceImpl();
		ServiceRegistration<Object> testServiceReg = registerTestService(testService);

		TestService service = tracker.waitForService(1000l);
		Assert.assertNotNull(service);

		try {
			service.test();
		} finally {
			txManagerServiceReg.unregister();
			testServiceReg.unregister();

			Mockito.verify(mock, Mockito.times(1)).begin();
			Mockito.verify(mock, Mockito.times(1)).setRollbackOnly();
			Mockito.verify(mock, Mockito.times(1)).commit();
		}

	}

	@Test(expected = IOException.class)
	public void testThrowsCheckedException() throws Exception {

		TransactionManager mock = Mockito.mock(TransactionManager.class);
		Mockito.when(mock.getStatus()).thenReturn(Status.STATUS_UNKNOWN).thenReturn(Status.STATUS_ACTIVE);
		ServiceRegistration<TransactionManager> txManagerServiceReg = context.registerService(TransactionManager.class,
				mock, null);

		TestService testService = new CheckedBrokenTestServiceImpl();
		ServiceRegistration<Object> testServiceReg = registerTestService(testService);

		TestService service = tracker.waitForService(1000l);
		Assert.assertNotNull(service);

		try {
			service.test();
		} finally {
			txManagerServiceReg.unregister();
			testServiceReg.unregister();

			Mockito.verify(mock, Mockito.times(1)).begin();
			Mockito.verify(mock, Mockito.times(0)).setRollbackOnly();
			Mockito.verify(mock, Mockito.times(1)).commit();
		}

	}

}
