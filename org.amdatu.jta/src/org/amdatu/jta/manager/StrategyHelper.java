/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager;

import java.lang.reflect.Method;

import org.amdatu.jta.Transactional;
import org.amdatu.jta.Transactional.TxType;

public class StrategyHelper {

	public static TxType determineStrategy(Method method, Class<?> clazz) {
		TxType strategy = determineTypeStrategy(clazz);
		if (strategy == null) {
			for (Class<?> interfaceClazz : clazz.getInterfaces()) {
				try {
					interfaceClazz.getMethod(method.getName(), method.getParameterTypes());
					strategy = determineTypeStrategy(interfaceClazz);
					if (strategy != null) {
						break;
					}
				} catch (NoSuchMethodException e) {
					// Do nothing
				}
			}
		}

		TxType methodStrategy = determineMethodStrategy(method, clazz);

		if (methodStrategy == null) {
			for (Class<?> interfaceClazz : clazz.getInterfaces()) {
				methodStrategy = determineMethodStrategy(method, interfaceClazz);
				if (methodStrategy != null) {
					break;
				}
			}
		}

		return methodStrategy != null ? methodStrategy : strategy;
	}

	private static TxType determineTypeStrategy(Class<?> clazz) {
		Transactional annotation = clazz.getAnnotation(Transactional.class);
		if (annotation != null) {
			if (annotation.value() != null) {
				return annotation.value();
			} else {
				return TxType.REQUIRED;
			}
		}

		if (clazz.getSuperclass() != null) {
			return determineTypeStrategy(clazz.getSuperclass());
		} else {
			return null;
		}
	}

	private static TxType determineMethodStrategy(Method method, Class<?> clazz) {

		try {
			Method implementationMethod = clazz.getMethod(method.getName(), method.getParameterTypes());
			Transactional annotation = implementationMethod.getAnnotation(Transactional.class);
			if (annotation != null) {
				if (annotation.value() != null) {
					return annotation.value();
				}
			}
		} catch (NoSuchMethodException e) {
			// Do nothing
		}

		if (clazz.getSuperclass() != null) {
			return determineMethodStrategy(method, clazz.getSuperclass());
		} else {
			return null;
		}
	}

}
