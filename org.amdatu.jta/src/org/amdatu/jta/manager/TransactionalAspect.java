/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.transaction.InvalidTransactionException;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionRequiredException;

import org.amdatu.jta.Transactional.TxType;
import org.osgi.service.log.LogService;

public class TransactionalAspect implements InvocationHandler {

	private volatile LogService logService;

	private volatile Object service;

	private volatile TransactionManager transactionManager;

	public TransactionalAspect(Object service) {
		this.service = service;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		if (Object.class == method.getDeclaringClass()) {
			String name = method.getName();
			if ("equals".equals(name)) {
				return proxy == args[0];
			} else if ("hashCode".equals(name)) {
				return System.identityHashCode(proxy);
			} else if ("toString".equals(name)) {
				return proxy.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(proxy))
						+ ", with InvocationHandler " + this;
			} else {
				throw new IllegalStateException(String.valueOf(method));
			}
		}

		TxType txType = StrategyHelper.determineStrategy(method, service.getClass());

		if (txType == null) {
			return method.invoke(service, args);
		}

		Transaction transaction = null;
		switch (txType) {
		case SUPPORTS:
			try {
				return method.invoke(service, args);
			} catch (InvocationTargetException e) {
				if (e.getTargetException() instanceof RuntimeException) {
					if (Status.STATUS_ACTIVE == transactionManager.getStatus()) {
						transactionManager.setRollbackOnly();
					}
				}
				throw e.getTargetException();
			}

		case MANDATORY:
			if (Status.STATUS_ACTIVE != transactionManager.getStatus()) {
				throw new TransactionRequiredException();
			}

			try {
				return method.invoke(service, args);
			} catch (InvocationTargetException e) {
				if (e.getTargetException() instanceof RuntimeException) {
					transactionManager.setRollbackOnly();
				}
				throw e.getTargetException();
			}

		case NEVER:
			if (Status.STATUS_ACTIVE == transactionManager.getStatus()) {
				throw new InvalidTransactionException();
			}
			return method.invoke(service, args);

		case NOT_SUPPORTED:
			if (Status.STATUS_ACTIVE == transactionManager.getStatus()) {
				transaction = transactionManager.suspend();
			}
			try {
				return method.invoke(service, args);
			} catch (InvocationTargetException e) {
				throw e.getTargetException();
			} finally {
				if (transaction != null) {
					transactionManager.resume(transaction);
				}
			}

		case REQUIRED:
			boolean transactionCreated = false;
			if (Status.STATUS_ACTIVE != transactionManager.getStatus()) {
				transactionManager.begin();
				transactionCreated = true;
			}

			try {
				return method.invoke(service, args);
			} catch (InvocationTargetException e) {
				if (e.getTargetException() instanceof RuntimeException) {
					transactionManager.setRollbackOnly();
				}
				throw e.getTargetException();
			} finally {
				if (transactionCreated) {
					try {
						if (transactionManager.getStatus() == Status.STATUS_ACTIVE) {
							transactionManager.commit();
						} else {
							transactionManager.rollback();
						}
					} catch (Throwable t) {
						logService.log(LogService.LOG_ERROR, "Failed to complete transaction ", t);
					}
				}
			}

		case REQUIRES_NEW:
			if (Status.STATUS_ACTIVE == transactionManager.getStatus()) {
				transaction = transactionManager.suspend();
			}
			transactionManager.begin();
			try {
				return method.invoke(service, args);
			} catch (InvocationTargetException e) {
				if (e.getTargetException() instanceof RuntimeException) {
					transactionManager.setRollbackOnly();
				}
				throw e.getTargetException();
			} finally {
				try {
					if (transactionManager.getStatus() == Status.STATUS_ACTIVE) {
						transactionManager.commit();
					} else {
						transactionManager.rollback();
					}
				} catch (Throwable t) {
					logService.log(LogService.LOG_ERROR, "Failed to complete transaction ", t);
				}

				if (transaction != null) {
					transactionManager.resume(transaction);
				}
			}
		default:
			throw new IllegalStateException("Transaction type " + txType + " not supported");
		}
	}
}
