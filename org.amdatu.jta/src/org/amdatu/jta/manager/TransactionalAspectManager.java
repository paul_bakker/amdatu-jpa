/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.transaction.TransactionManager;

import org.amdatu.jta.ManagedTransactional;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

public class TransactionalAspectManager {

	private volatile DependencyManager dependencyManager;

	private volatile Map<ServiceReference<?>, Component> map = new ConcurrentHashMap<>();

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onAdded(ServiceReference<?> ref, Object service) {
		try {

			Class<?> serviceClass = service.getClass();

			Object ta = Proxy.newProxyInstance(serviceClass.getClassLoader(), serviceClass.getInterfaces(),
					new TransactionalAspect(service));

			Properties properties = new Properties();

			for (String key : ref.getPropertyKeys()) {
				if (Constants.OBJECTCLASS.equals(key)) {
					continue;
				}

				if ("transactional".equals(key)) {
					continue;
				}
				
				Object value = ref.getProperty(key);
				properties.put(key, value);
			}

			properties.put("isManaged", true);
			
			String interfaceClass;
			
			Class<?>[] interfaces = service.getClass().getInterfaces();
			Set<Class<?>> interfacesSet = new HashSet<>(Arrays.asList(interfaces));
			
			if(interfacesSet.contains(ManagedTransactional.class)) {
				interfaceClass = ((ManagedTransactional)service).getManagedInterface().getName();
			} else {
				interfaceClass = (String)ref.getProperty("transactional");
			}
			
			if(interfaceClass == null) {
				throw new RuntimeException("Invalid managed transaction component registration, no interface set");
			}
			
			Component component = dependencyManager
					.createComponent()
					.setInterface(interfaceClass, properties)
					.setImplementation(ta)
					.add(dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false))
					.add(dependencyManager.createServiceDependency().setService(TransactionManager.class)
							.setRequired(true));

			map.put(ref, component);

			dependencyManager.add(component);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onRemoved(ServiceReference<?> ref, Object service) {
		Component component = map.remove(ref);
		if (component != null) {
			dependencyManager.remove(component);
		}
	}

}
