/**
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.ditp.lego.jta.manager;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

import org.amdatu.jta.Transactional;
import org.amdatu.jta.Transactional.TxType;
import org.amdatu.jta.manager.StrategyHelper;
import org.junit.Test;

public class StrategyHelperTest {

	@Test
	public void noStrategy() throws NoSuchMethodException, SecurityException{
		
		Method method = NonTransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, NonTransactionalService.class);
		assertNull(strategy);
		
	}
	
	interface NonTransactionalInterface {
		
		void method();
		
	} 
	
	class NonTransactionalService implements NonTransactionalInterface {

		@Override
		public void method() {
			// do nothing
		}
		
	}

	@Test
	public void transactionalImplementationDefaultStrategy() throws NoSuchMethodException, SecurityException{
		Method method = NonTransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, TransactionalServiceImplementation.class);
		assertEquals(TxType.REQUIRED, strategy);
		
	}
	
	@Transactional
	class TransactionalServiceImplementation implements NonTransactionalInterface{

		@Override
		public void method() {
			// do nothing
		}
	}
	
	
	@Test
	public void transactionalMethodImplementationDefaultStrategy() throws NoSuchMethodException, SecurityException{
		Method method = NonTransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, TransactionalMethodImplementation.class);
		assertEquals(TxType.REQUIRED, strategy);
		
	}
	
	class TransactionalMethodImplementation implements NonTransactionalInterface{

		@Override
		@Transactional
		public void method() {
			// do nothing
		}
	}
	
	@Test
	public void transactionalMethodOverridesTypeImplementationDefaultStrategy() throws NoSuchMethodException, SecurityException{
		Method method = NonTransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, MethodOverridesImplementation.class);
		assertEquals(TxType.NOT_SUPPORTED, strategy);
		
	}
	
	@Transactional
	class MethodOverridesImplementation implements NonTransactionalInterface{

		@Override
		@Transactional(TxType.NOT_SUPPORTED)
		public void method() {
			//
		}
		
	}
	
	@Test
	public void transactionalInterfaceDefaultStrategy() throws NoSuchMethodException, SecurityException{
		Method method = TransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, InheritInterfaceTransactionType.class);
		assertEquals(TxType.REQUIRED, strategy);	
	}
	
	@Transactional
	interface TransactionalInterface {
		
		void method();
		
	}
	
	class InheritInterfaceTransactionType implements TransactionalInterface {

		@Override
		public void method() {
			// 
		}
		
	}
	
	@Test
	public void implementationBeforeInterface() throws NoSuchMethodException, SecurityException{
		Method method = TransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, ImplementationBeforeInterface.class);
		assertEquals(TxType.MANDATORY, strategy);	
	}
	
	@Transactional(TxType.MANDATORY)
	class ImplementationBeforeInterface implements TransactionalInterface {

		@Override
		public void method() {
			
		}
		
	}
	
	@Test
	public void superBeforeInterface() throws NoSuchMethodException, SecurityException{
		Method method = TransactionalInterface.class.getMethod("method");
		TxType strategy = StrategyHelper.determineStrategy(method, SuperBeforeInterface.class);
		assertEquals(TxType.MANDATORY, strategy);	
	}
	
	@Transactional(TxType.MANDATORY)
	class SuperBeforeInterfaceSuper implements TransactionalInterface {

		@Override
		public void method() {
			// TODO Auto-generated method stub
			
		}
	}
	
	class SuperBeforeInterface extends SuperBeforeInterfaceSuper{ 
		
	}
	

}


